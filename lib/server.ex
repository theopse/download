# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# This File if From Theopse (Self@theopse.org)
# Licensed under BSD-3-Caluse
# File:	server.ex (download/lib/server.ex)
# Content:
# Copyright (c) 2021 Theopse Organization All rights reserved
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

defmodule Download.Server do
  import Standard

  begin "Dependance Require" do
    import Download.HTTP
  end

  begin "Function Defination" do
    begin :_ do
      begin {:__loop__, 3} do
        def __loop__(state, ref, fun) do
          receive do
            {from, ^ref, :change, new_fun} ->
              send(from, {self(), ref, :ok})
              __loop__(state, ref, new_fun)

            {from, ^ref, :call, arg} ->
              result = fun.(state, arg)
              send(from, {self(), ref, {:ok, result}})
              __loop__(state, ref, fun)

            {from, ^ref, :store, data} ->
              send(self(), {:stored, data})
              send(from, {self(), ref, {:ok, data}})
              __loop__(state, ref, fun)

            {from, ^ref, :reply} ->
              receive do
                {:stored, data} ->
                  send(from, {self(), ref, {:ok, data}})
              after
                0 ->
                  send(from, {self(), ref, {:error, :none}})
              end

              __loop__(state, ref, fun)

            {from, ^ref, :cast, arg} ->
              send(from, {self(), ref, :ok})
              fun.(state, arg)
              __loop__(state, ref, fun)

            {from, ^ref, :stop} ->
              send(from, {self(), ref, :ok})
              exit(:STOP)

            {from, ^ref, :is_lock} ->
              send(from, {self(), ref, false})
              __loop__(state, ref, fun)
              # {from, ^ref, :lock} ->
              # send(from, {self(), ref, :ok})
              # __lock_loop__(state, ref, fun, [])
          end
        end
      end
    end

    begin A do
      begin {:async, [1, 2]} do
        def async(url, otps \\ []) do
          self = :erlang.self()
          ref = :erlang.make_ref()
          headers = headers(otps)

          fun = fn _, url ->
            HTTPoison.get(url, headers, ssl())
            |> case do
              {:ok, %HTTPoison.Response{body: body}} ->
                send(self(), {:stored, body})

              {:error, _} ->
                send(self(), {:stored, :error})
            end
          end

          pid = spawn(__MODULE__, :__loop__, [nil, ref, fun])

          send(pid, {self, ref, :cast, url})

          {:server, pid, ref}
        end
      end

      begin {:await, [1, 2]} do
        def await({:server, pid, ref}, timeout \\ 5000) do
          send(pid, {self(), ref, :reply})

          receive do
            {^pid, ^ref, {:ok, data}} ->
              send(pid, {self(), ref, :stop})
              data
          after
            timeout ->
              send(pid, {self(), ref, :stop})
              {:error}
          end
        end
      end
    end

    begin S do
      begin {:stream, [1, 2]} do
        def stream(item, otps \\ [])

        def stream(
              %Download.Stream{url: url, start: start_fn, content: content_fn,error: error_fn, final: final_fn},
              otps
            ) 
        do
          self = :erlang.self()
          ref = :erlang.make_ref()
          headers = headers(otps)

          recv = fn state, %HTTPoison.AsyncResponse{id: id} = resp ->
            receive do
              %HTTPoison.AsyncStatus{id: ^id, code: 302} ->
                HTTPoison.stream_next(resp)

                receive do
                  %HTTPoison.AsyncHeaders{headers: headers, id: ^id} ->
                    {"Location", url} = List.keyfind(headers, "Location", 0)
                    {:reset, url}
                end

              status = %HTTPoison.AsyncStatus{id: ^id, code: 200} ->
                HTTPoison.stream_next(resp)
                content_fn.(state, status)
                :next

              status = %HTTPoison.AsyncHeaders{id: ^id} ->
                HTTPoison.stream_next(resp)
                content_fn.(state, status)
                :next

              status = %HTTPoison.AsyncChunk{id: ^id, chunk: chunk} ->
                HTTPoison.stream_next(resp)
                content_fn.(state, status)
                chunk

              status = %HTTPoison.AsyncEnd{id: ^id} ->
                content_fn.(state, status)
                :halt

              status ->
                error_fn.(state, status)
                :halt
            end
          end

          start = fn ->
            pid = spawn(__MODULE__, :__loop__, [nil, ref, recv])

            {:ok, resp} =
              HTTPoison.get(
                url,
                headers,
                ssl: [{:versions, [:"tlsv1.2", :"tlsv1.1", :tlsv1]}],
                recv_timeout: :infinity,
                stream_to: pid,
                async: :once
              )

            start_fn.(resp)

            {pid, ref, resp}
          end

          handle = fn arg = {pid, ref, resp} ->
            send(pid, {self, ref, :call, resp})

            receive do
              {^pid, ^ref, {:ok, result}} ->
                case result do
                  :halt ->
                    {:halt, arg}

                  :next ->
                    {[], arg}

                  {:reset, url} ->
                    :hackney.stop_async(resp.id)

                    {:ok, new_resp} =
                      HTTPoison.get(
                        url,
                        headers,
                        ssl: [{:versions, [:"tlsv1.2", :"tlsv1.1", :tlsv1]}],
                        recv_timeout: :infinity,
                        stream_to: pid,
                        async: :once
                      )

                    {[], {pid, ref, new_resp}}

                  chunk ->
                    {[chunk], arg}
                end
            end
          end

          ends = fn {pid, ref, %HTTPoison.AsyncResponse{id: id}} ->
            :hackney.stop_async(id)
            final_fn.()
            send(pid, {self, ref, :stop})
          end

          Stream.resource(start, handle, ends)
        end

        def stream(url, otps), do: %Download.Stream{url: url, start: fn _ -> nil end, content: fn _, _ -> nil end, error: fn _, item -> IO.inspect(item) end,  final: fn -> nil end} |> stream(otps)

        left do
          def stream(url, otps) do
            self = :erlang.self()
            ref = :erlang.make_ref()
            headers = headers(otps)

            recv = fn _, %HTTPoison.AsyncResponse{id: id} = resp ->
              receive do
                %HTTPoison.AsyncStatus{id: ^id, code: 302} ->
                  HTTPoison.stream_next(resp)

                  receive do
                    %HTTPoison.AsyncHeaders{headers: headers, id: ^id} ->
                      {"Location", url} = List.keyfind(headers, "Location", 0)
                      {:reset, url}
                  end

                %HTTPoison.AsyncStatus{id: ^id, code: 200} ->
                  HTTPoison.stream_next(resp)
                  :next

                %HTTPoison.AsyncHeaders{id: ^id} ->
                  HTTPoison.stream_next(resp)
                  :next

                %HTTPoison.AsyncChunk{id: ^id, chunk: chunk} ->
                  HTTPoison.stream_next(resp)
                  chunk

                %HTTPoison.AsyncEnd{id: ^id} ->
                  :halt

                item ->
                  IO.inspect(item)
                  :halt
              end
            end

            start = fn ->
              pid = spawn(__MODULE__, :__loop__, [nil, ref, recv])

              {:ok, resp} =
                HTTPoison.get(
                  url,
                  headers,
                  ssl: [{:versions, [:"tlsv1.2", :"tlsv1.1", :tlsv1]}],
                  recv_timeout: :infinity,
                  stream_to: pid,
                  async: :once
                )

              {pid, ref, resp}
            end

            handle = fn arg = {pid, ref, resp} ->
              send(pid, {self, ref, :call, resp})

              receive do
                {^pid, ^ref, {:ok, result}} ->
                  case result do
                    :halt ->
                      {:halt, arg}

                    :next ->
                      {[], arg}

                    {:reset, url} ->
                      :hackney.stop_async(resp.id)

                      {:ok, new_resp} =
                        HTTPoison.get(
                          url,
                          headers,
                          ssl: [{:versions, [:"tlsv1.2", :"tlsv1.1", :tlsv1]}],
                          recv_timeout: :infinity,
                          stream_to: pid,
                          async: :once
                        )

                      {[], {pid, ref, new_resp}}

                    chunk ->
                      {[chunk], arg}
                  end
              end
            end

            ends = fn {pid, ref, %HTTPoison.AsyncResponse{id: id}} ->
              :hackney.stop_async(id)
              send(pid, {self, ref, :stop})
            end

            Stream.resource(start, handle, ends)
          end
        end
      end
    end
  end
end
